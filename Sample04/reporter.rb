require 'report_builder'
require 'json'
require 'peach'
# require 'rbconfig'
require 'loofah'
require_relative 'scrubbers.rb'
# Prepare Json report files for html report and generate html eport
class Reporter
    
  def move_failed_reports_to_seperate_files
    reports_files = Dir['reports/*.json']
    (0..reports_files.size - 1).each do |i|
      begin
        report = File.read(reports_files[i])
        move_failed_test_to_seperate_file(report);
      rescue => e
        print e
      end
    end	
	
   def move_failed_report_to_seperate_file(file)
		report = JSON.parse(file)
        empty_feature_array = []
        report.each do |feature|
          failed_array = []
          feature['elements'].each do |element|
            begin
              %w(before steps after).each do |element_name|
                add_to_failure_scenarios_for(element, element_name, failed_array)
              end
            rescue NoMethodError => e
              print e.message.to_s
            end
          end
          failed_array.each do |index|
            feature['elements'].delete(index)
          end
          empty_feature_array.push(feature) if feature['elements'].empty?
        end
        empty_feature_array.each do |index|
          data_hash.delete(index)
        end
        File.open(report_files[i], 'w') do |result_file|
          result_file.write(data_hash.to_json)
        end
	end
  end


  private def add_to_failure_scenarios_for(element, element_name, failed_array)
    element[element_name].each do |step|
      if step['result']['status'] == 'failed'
        failed_array.push(element)
        break
      end
    end
  end

  def add_browser_name_to_jsons
    reports_array = Dir['reports/*.json']
    (0..reports_array.size - 1).each do |i|
      begin
        file = File.read(reports_array[i])
        data_hash = JSON.parse(file)
        add_browser_name_for(reports_array[i], data_hash)
        File.open(reports_array[i], 'w') do |result_file|
          result_file.write(data_hash.to_json)
        end
      rescue => e
        print e
      end
    end
  end

  private def add_browser_name_for(browser_name, data_hash)
    report_name = browser_name.split('.')[0].split('/')[1].gsub(/--\d{0,2}/, '')
    data_hash.each do |feature|
      feature['name'] = report_name + ' ' + feature['name']
      feature['id'] = report_name + feature['id']
      feature['elements'].each do |element|
        element['name'] = report_name + ' ' + element['name']
        temp_id = element['id'].split(';')
        element['id'] = temp_id[0] + ';' + report_name + temp_id[1]
      end
    end
  end

  def detect_failure
    failed = false
    reports_array = Dir['reports/*.json']
    (0..reports_array.size - 1).each do |i|
      begin
        JSON.parse(File.read(reports_array[i])).each do |feature|
          feature['elements'].each do |element|
            begin
              %w(before steps after).each do |element_name|
                element[element_name].each do |step|
                  if step['result']['status'] == 'failed'
                    failed = true
                    break
                  end
                end
                break if failed
              end
            rescue NoMethodError => e
              print e.message.to_s
            end
            break if failed
          end
          break if failed
        end
      rescue => e
        print e
      end
      break if failed
    end
    return failed
  end

  def generate_report(options)
    ReportBuilder.configure do |config|
      config.json_path = 'reports'
      config.report_path = 'reports/report'
      config.report_types = [:html]
      config.report_tabs = [:overview, :features, :scenarios, :errors]
      config.report_title = 'Mobile Automation'
      config.compress_images = false
    end
    ReportBuilder.build_report
  end

  def remove_screenshots_if_too_large(options)
    html_size = (File.size(options['report_path'] + '.html').to_f / 2**20).round(2)
    if html_size > 250.0
      scrubber = PermitScrubber.new
      scrubber.tags = ['img']
      doc = File.open(options['report_path'] + '.html')
      html_fragment = Loofah.document(doc)
      html_fragment.scrub!(scrubber)
      File.write(options['report_path'] + '.html', html_fragment)
    end
  end
end
