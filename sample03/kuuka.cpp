#include <iostream>
#include <math.h>
#include <string.h>

using namespace std;

void p1( int n, char x ) {
    for (int i = 0; i < n; i++) cout << x;
    cout << "\n";
}
void p2( int n, char x ) {
    for (int i = 0; i < n; i++) {
        cout << x;
        if (i != n-1) cout << " ";
    }
    cout << "\n";
}
int main()
{
    int n;

    cin >> n;

    p2( n, '$');
    p2( n, '|');
    int m = (2*n)-1;
    p1( m, '-');
    p1( m, '~');
    p1( m, '-');
}